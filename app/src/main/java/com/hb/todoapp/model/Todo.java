package com.hb.todoapp.model;


import android.graphics.Color;

public class Todo {


    private String title;
    private String description;
    private State state;
    private int color;

    public Todo(String title) {
        this(title, "");
    }

    public Todo(String title, String description) {
        this.title = title;
        this.state = State.ACTIVE;
        this.color = Color.BLUE;
        this.description = description;
    }
    public Todo(String title, int c) {
        this.title = title;
        this.color = c;
        this.state = State.ACTIVE;
    }

    public String getDescription() {
        return description;
    }

    public Boolean isActive() {
        if (this.state == State.ACTIVE) {
            return true;
        }
        return false;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setCompleted() {
        this.state = State.COMPLETED;
    }

    public String getColorAsHex() {
        return "#" + Integer.toHexString(this.color);
    }

    @Override
    public String toString() {
        return "Todo{" +
                "title='" + title + '\'' +
                ", state=" + state +
                ", color=" + this.getColorAsHex() +
                '}';
    }

    public enum State {
        COMPLETED,
        ACTIVE
    }
}
