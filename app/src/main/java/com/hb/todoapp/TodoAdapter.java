package com.hb.todoapp;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.hb.todoapp.model.Todo;

import java.util.ArrayList;
import java.util.List;

public class TodoAdapter extends BaseAdapter{

    private ArrayList<Todo> todoList;
    private ArrayList<Todo> todoListActive;


    private Context context;
    private boolean filterActive;

    public TodoAdapter(Context context, ArrayList<Todo> list) {
        this.context = context;
        this.todoList = list;
    }

    public ArrayList<Todo> getTodoList() {
        if (this.filterActive) {
            this.todoListActive = new ArrayList<>();
            for (Todo todo : this.todoList) {
                if (todo.isActive())
                    this.todoListActive.add(todo);
            }
            return this.todoListActive;
        }
        else {
            return this.todoList;
        }
    }

    // Ajouter un t odo dans la liste
    public void add(Todo todo) {
        todoList.add(todo);
        // notifyDataSetChanged => on signale que les données ont changé
        // et donc cela va "recharger" la listView
        notifyDataSetChanged();
    }

    // Nombre d'éléments dans ma liste
    // Il faut le surchager la méthode car on a pas tout le temps
    // des arraylist
    @Override
    public int getCount() {
        return this.getTodoList().size();
    }

    // D'obtenir le T odo à la position index
    @Override
    public Object getItem(int index) {
        return this.getTodoList().get(index);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    // Cette méthode getView va être appelée automatiquement à chaque fois
    // qu'une ligne est affichée à l'écran
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // LinearLayout correspond à l'élement racine de row_todo
        LinearLayout linearLayout = null;
        if (convertView != null) {
            linearLayout = (LinearLayout) convertView;
        }
        else {
            linearLayout = (LinearLayout) LayoutInflater.from(context).
                    inflate(R.layout.row_todo, parent, false);
        }

        // Je récupère quel t odo on est entrain d'afficher
        final Todo currentTodo = (Todo) getItem(position);

        // Ici je dois "remplir" les élements graphiques de mon row_todo

        TextView txtTitle = (TextView) linearLayout.findViewById(R.id.txtTitle);
        final TextView txtDescription = (TextView) linearLayout.findViewById(R.id.txtDesc);
        Switch btnSwitch = (Switch) linearLayout.findViewById(R.id.switch1);
        Button btnDelete = (Button) linearLayout.findViewById(R.id.btnRemove);


        // Le titre
        txtTitle.setText(currentTodo.getTitle());
        // La couleur du titre
        txtTitle.setTextColor(currentTodo.getColor());

        // La description
        txtDescription.setText(currentTodo.getDescription());
        Log.d("DEBUG", "pos " + position + " " + currentTodo.getTitle() + " " + currentTodo.isActive());
        /*if (this.filterActive && !currentTodo.isActive()) {
            System.out.println("la");
            linearLayout.setVisibility(View.GONE);
        }*/
        if (currentTodo.isActive()) {
            btnSwitch.setChecked(true);
            linearLayout.setAlpha(1f);
        } else {
            btnSwitch.setChecked(false);
            linearLayout.setAlpha(0.5f);
        }
        // Le switch qui correspond au btnSwitch du t odo

        // je veux capturé un evenement quand l'utlisateur click sur le bouton switch
        btnSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            // Ici je suis dans une classe anonyme
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                LinearLayout l = (LinearLayout) buttonView.getParent();

                if(isChecked == true) {
                    // le switch est "checked"
                    // je vais donc mettre à jours la T-odo correspondante
                    currentTodo.setState(Todo.State.ACTIVE);
                    l.setAlpha(1f);
                }
                else {
                    currentTodo.setCompleted();
                    l.setAlpha(0.5f);
                }
            }
        });

        // Le bouton supprimer
        // btnDelete.setOnClickListener(deleteListener);
        btnDelete.setTag(linearLayout);
        btnDelete.setOnClickListener(deleteListener);


        return linearLayout;
    }


    private View.OnClickListener deleteListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Todo currentTodo = (Todo)v.getTag();
            System.out.println(currentTodo.getTitle());
            //LinearLayout l = (LinearLayout) v.getParent();

            // LinearLayout ll = (LinearLayout) v.getParent();
            // TextView txtTitle = (TextView) ll.findViewById(R.id.txtTitle);
            // System.out.println(txtTitle.getText().toString());
        }
    };


    public void filterByActive() {
        this.filterActive = true;
        // this.todoList = new ArrayList<>()
        notifyDataSetChanged();
    }

    public void removeFilter() {
        this.filterActive = false;
        notifyDataSetChanged();
    }
}
